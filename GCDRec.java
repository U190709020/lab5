public class GCDRec {
    public static void main(String[] args) {
        int number_1 = Integer.parseInt(args[0]);
        int number_2 = Integer.parseInt(args[1]);
        int GCD = GCD(number_1, number_2);

        System.out.print("G.C.D of " + number_1 + " and " + number_2 + " is " + GCD);
    }

    public static int GCD(int n1, int n2)
    {
        if (n2 != 0)
            return GCD(n2, n1 % n2);
        else
            return n1;
    }
}
