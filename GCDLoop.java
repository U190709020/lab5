public class GCDLoop {

    public static void main(String[] args) {
        int Number_1 = Integer.parseInt(args[0]);
        int Number_2 = Integer.parseInt(args[1]);
        int GCD = 0;
        int GCD2 = 0;

        if (Number_1 > Number_2) {
            int x = Number_1 % Number_2 ;
            while (true){
               if (x != 0) {
                   GCD = x;
                   GCD2 = Number_2 % GCD;
                   x = GCD2;
               }
               else {
                   break;
               }
            }
        }
        else if (Number_1 == Number_2){
            GCD = Number_1;
        }
        else {
            int x = Number_2 % Number_1 ;
            while (true){
                if (x != 0) {
                    GCD = x;
                    GCD2 = Number_1 % GCD;
                    x = GCD2;
                }
                else {
                    break;
                }
            }
        }
        System.out.print("GCD is " + GCD);
    }
}
